# Projet-GeoffreyDolleans-YassineBoujerfaoui-JosephFaudou-ClementCote

# Description du projet
Modéliser un traffic routier avec des intersections et des comportements aléatoires, à travers un automate cellulaire

# MVP
Le premier MVP se trouve dans le dossier mvp, les instructions sont dans mvp/README.md
Le MVP le plus abouti correspond au dossier mvp_virages_interface, il contient toutes les fonctions dans leur version finale et leur test.
Les instruction sont dans le README.md

# Fonctionnalités supplémentaires par rapport au premier mvp
- MVP virage : ajout de la gestion d'une vitesse supérieure à 1 case par itération, possibilité d'avoir des virages, ajout du freinage aléatoire
- MVP interface : ajout d'une interface graphique pour générer un univers aléatoire
- MVP virage interface : concaténation des deux points précédents
- MVP interface génération : ajout par rapport au MVP la possibilité de générer un univers personnalisé via une interface graphique

# Dossier programmes
Contient tous les programmes dans leur première version fonctionnelle

# Sprints et fonctionnalités
- Sprint 1 : générer des routes, générer des voitures, afficher routes et voitures, gestion des intersections
- Sprint 2 : programmer le comportement des voitures
- Sprint 3 : ajout de types de comportements et d'intersections
- Sprint 4 : interface graphique

# Première répartition
- Geoffrey Dolleans : affichage
- Yassine Boujerfaoui : classe voiture
- Joseph Faudou : intersection
- Clément Cote : classe route

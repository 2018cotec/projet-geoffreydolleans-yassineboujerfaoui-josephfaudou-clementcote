# Variables globales
- Liste de liste de la position des voitures : position_voitures (0 si pas de voiture)

- Liste de liste de la position des routes : position_routes

- Liste de la position des fins de routes : pos_fin_routes

- Taille de l'univers : taille

- La position (x,y) correspond à tableau[y][x]

# Comportement des voitures
- Si une voiture ne freine pas, et qu'elle n'est pas gênée par une autre voiture, alors elle accélère jusqu'à la vitesse maximale

# Classe voiture
	Attributs :

		postition : position=(x,y)

		vitesse : v

		probabilité de freinage : p
		
		direction : str

# Classe espace
	Attributs :

		type : 0 si rien, 1 si route, 2 si intersection

		position x,y

		type_inter : "non", "priorité à droite", "feu rouge"

		direction : "haut","bas","gauche","droite","non"

		v_lim : limitation de vitesse

		p_arrivée : probabilité qu'une voiture apparaisse sur cette case (au bord de l'espace)

#processus mouvement
On remonte chaque route en ajustant la vitesse de chaque voiture, appelle d'intersection si nécessaire
Avec toute les vitesses et directions definient on avancent les voitures

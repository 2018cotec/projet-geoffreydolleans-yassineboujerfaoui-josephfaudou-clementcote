# MVP
Exécuter le fichier main.py

# Que fait le MVP ?
Le MVP simule la circulation sur un univers de taille 50*50, sur lequel sont placées aléatoirement 6 routes rectilignes. Les intersections sont aléatoirement des priorités à droite ou des feux rouges. Les feux rouges sont affichés en rouge et les priorités à droite sont affichées en vert. Des voitures arrivent depuis le début d'une route avec une probabilité 0,1. Toutes les voitures sont soit à l'arrêt, soit avancent d'une case par itération.

# Dépendances
- Matplotlib

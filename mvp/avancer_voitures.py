#fonction qui prend en argument l'univers avec les voitures et renvoie un nouvel univers avec les voitures dans leur nouvelle position

def avancer_voitures(position_voitures):
    """
    :param position_voitures: liste contenant les voitures à l'instant t
    :return: new_position_voiture: liste contenant les voitures à l'instant t+1
    """
    taille=len(position_voitures)
    new_position_voiture=[[0 for _ in range(taille)] for _ in range (taille)] #créer un nouvel univers vide
    for ligne in position_voitures:
        for voiture in ligne:
            if voiture!=0:
                x, y = voiture.position
                if voiture.direction=='haut': #modifie la position de la voiture en fonction de sa direction et de sa vitesse
                    y -= voiture.v
                elif voiture.direction=='bas':
                    y += voiture.v
                elif voiture.direction=='droite':
                    x += voiture.v
                else:
                    x -= voiture.v
                voiture.position = (x, y)
                if 0<=voiture.position[0]<=taille-1 and 0<=voiture.position[1]<=taille-1 :
                    new_position_voiture[voiture.position[1]][voiture.position[0]]=voiture #ajoute la voiture au nouvel univers si elle est dedans

    return new_position_voiture



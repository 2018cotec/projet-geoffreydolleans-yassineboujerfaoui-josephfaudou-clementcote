import Animation
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import generer_voiture
import Espace
from matplotlib.colors import LinearSegmentedColormap

# Initialisation des variables
n = 0
position_routes, depart_routes, fin_routes = Espace.generer_espace(50, 6)
position_voitures = [[0 for _ in range(50)] for _ in range(50)]
generer_voiture.generer_voiture(position_voitures, depart_routes, position_routes, 0.3)

# Création de la colormap personnalisée
colormap=LinearSegmentedColormap.from_list('CustomColormap',[(1,1,1),(0,1,0),(1,0,0),(0,0,0)])

# Création de la figure et de la première image de l'animation
fig = plt.figure()
im = plt.imshow(Animation.generation_image(position_routes, position_voitures), cmap=colormap, animated=True)


# Définition de la fonction d'animation
def updatefig(*args):
    global position_voitures, position_routes, depart_routes, fin_routes, n
    position_voitures = Animation.next_step(position_routes, position_voitures, depart_routes, fin_routes, n)
    im.set_array(Animation.generation_image(position_routes, position_voitures))

    # Itération suivante
    n += 1
    return im,

ani = animation.FuncAnimation(fig, updatefig, interval=300, blit=True,repeat=False)
plt.show()

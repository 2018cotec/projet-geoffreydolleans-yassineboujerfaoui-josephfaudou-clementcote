# MVP_interface
Exécuter le fichier main.py

# Que fait le MVP ?
Le MVP ouvre une interface graphique permettant de choisir les paramètres de la simulationn :
    - taille de l'univers
    - nombre de routes
    - vitesse limite
    - type d'intersection
    - densité de circulation

# Dépendances
- Matplotlib
- tkinter

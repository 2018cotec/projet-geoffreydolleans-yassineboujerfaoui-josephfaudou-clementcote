import random as rd
import Classes

#fonction qui créer des voitures au début des routes
def generer_voiture(position_voitures,depart_route,position_routes,proba_apparition,proba_freinage=0.1):
    for depart in depart_route:
        if position_voitures[depart[1]][depart[0]]==0: #on vérifie que la place est libre
            p=rd.random()
            if p<=proba_apparition:         #une voiture arrive avec une certaine probabilité
                route=position_routes[depart[1]][depart[0]]
                v=rd.randint(1,route.v_lim)           #elle arrive avec une vitesse de 1
                position_voitures[depart[1]][depart[0]] = Classes.voiture((depart[0],depart[1]), v, position_routes, proba_freinage)  #ajout de l'objet voiture




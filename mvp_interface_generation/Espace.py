import random as rd

class Espace:
    """Classe qui définit une case d'espace caractérisée par :
    - sa position dans l'espace
    - son type (vide (0), route (1) ou intersection (2))
    - son type d'intersection ("non", "feu_rouge", "priorité_à_droite")
    - sa limitation de vitesse
    - la probabilité qu'une voiture apparaisse sur cette case (au bord de l'espace)
    """

    def __init__(self, x, y, type, direction, type_inter, v_lim=1, p_arrivee=0):
        self.position = (x, y)
        self.type = type
        self.type_inter = type_inter
        self.direction = direction
        self.v_lim = v_lim
        self.p_arrivee = p_arrivee


def generer_espace(taille, nombre_route):
    """
    :param taille: taille de l'espace que l'on va générer
    :param nombre_route: nombre de routes qui sera généré
    :return: routes : tableau de tableau contenant des objets espaces, depart_route : liste de tuples contenant les positions de départ des routes, fin_route : liste de tuples contenant les positions de fin des routes
    """

    # Génération du tableau
    routes = [[0 for i in range(taille)] for j in range(taille)]

    # Tableaux contenants respectivement les valeurs de départ sur x et sur y des routes
    x_start = []
    y_start = []

    # Tableaux contenants respectivement les coordonnées de départ et d'arrivée des routes
    depart_route = []
    fin_route = []

    for i in range(nombre_route):
        # Choix de la direction de la route
        depart = rd.choice(["x", "y"])

        if depart == 'x':
            # Choix du sens de la route
            x = rd.choice([0, taille - 1])

            # Choix de l'ordonnée de départ, et vérification qu'elle n'est pas adjacente à une route
            y = rd.randint(1, taille - 2)
            while y + 1 in y_start or y - 1 in y_start or y in y_start:
                y = rd.randint(1, taille - 2)

            # Ajout de la position de départ dans y_start
            y_start.append(y)

            # Fixe la valeur de la direction
            if x == 0:
                direction = "droite"
            else:
                direction = "gauche"

        # Equivalent au if précédent
        else:
            y = rd.choice([0, taille - 1])
            x = rd.randint(1, taille - 2)
            while x + 1 in x_start or x - 1 in x_start or x in x_start:
                x = rd.randint(1, taille - 2)
            x_start.append(x)
            if y == 0:
                direction = "bas"
            else:
                direction = "haut"

        # Ajout du point de départ de la route
        depart_route.append((x, y))

        # Ajout d'une distance maximale pour ne pas avoir de boucle infinie
        distance = 100 * taille
        j = 0

        # Boucle jusqu'à ce que la route touche le bout de l'espace
        while x < taille and y < taille and x >= 0 and y >= 0 and j < distance:

            # S'il n'y a pas déjà de route : crée une route
            if routes[y][x] == 0:
                routes[y][x] = Espace(x, y, 1, direction, "non")

            # S'il y a déjà une route, transforme la route existante en intersection
            else:
                routes[y][x].type = 2
                routes[y][x].direction = "non"
                if rd.random() < 1/2:
                    routes[y][x].type_inter = "priorité à droite"
                else:
                    routes[y][x].type_inter = "feu rouge"
                
            # chgt_direction=rd.random()
            # if chgt_direction<0.1 and (direction=="haut" or direction=="bas"):
            #     direction=rd.choice(["gauche","droite"])
            # elif chgt_direction<0.1 and (direction=="gauche" or direction=="droite"):
            #     direction=rd.choice(["haut","bas"])

            # On se déplace selon la direction
            if direction == "haut":
                y -= 1
            elif direction == "bas":
                y += 1
            elif direction == "gauche":
                x -= 1
            elif direction == "droite":
                x += 1

            j += 1

        # Ajout de la fin de la route : dépend de la direction
        if direction == "haut":
            fin_route.append((x, y + 1))
        elif direction == "bas":
            fin_route.append((x, y - 1))
        elif direction == "gauche":
            fin_route.append((x + 1, y))
        elif direction == "droite":
            fin_route.append((x - 1, y))

        # On remplit les cases qui ne sont pas des routes avec des espaces de type 0
    for x in range(taille):
        for y in range(taille):
            if routes[y][x] == 0:
                routes[y][x] = Espace(x, y, 0, "non", "non")

    return routes, depart_route, fin_route

if __name__=="__main__":
    import matplotlib.pyplot as plt
    routes, debut_route, fin_routes=generer_espace(50, 5)
    print(routes)
    print(debut_route)
    print(fin_routes)
    affichage=[[0 for _ in range(50)] for _ in range(50)]
    for i in range(50):
        for j in range(50):
            affichage[i][j]=routes[i][j].type
    print(affichage)
    plt.imshow(affichage,cmap='Greys')
    plt.show()




# Interface de génération
Exécuter le fichier main.py

# Que fait ce programme ?
Ce programme permet de générer un univers personnalisé d'une taille de 25*25 et le simule selon les règles de du MVP.

Les possibilités de modification des options de simulation sont :
- Les points de départ des routes. Les routes générées sont ensuite rectilignes.
- Le type de chaque intersection, entre priorité à droite et feu rouge.
- La densité de la cirdculation : 0 correspond à aucune voiture et 1 à des voitures se suivant de manière continue.

# Dépendances
- Matplotlib

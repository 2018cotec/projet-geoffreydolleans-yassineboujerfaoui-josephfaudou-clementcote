import tkinter as tk
from Animation import *
from Espace import *
from matplotlib.colors import LinearSegmentedColormap
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import generer_voiture

def generate_universe_from_gui():
    global main_frame_1,main_frame_2,window,checkboxes_2,checkboxes_variables_2,subframe_4,checkboxes_variables,routes,depart_routes,fin_routes
    routes=[[0 for _ in range(25)] for _ in range(25)]
    depart_routes=[]
    fin_routes=[]
    for x in range(1,24):
        for y in [0,24]:
            if y==0 and checkboxes_variables[y][x].get():
                direction='bas'

                # Ajout du point de départ de la route
                depart_routes.append((x, y))

                # Ajout d'une distance maximale pour ne pas avoir de boucle infinie
                distance = 100 * 25
                j = 0

                # Boucle jusqu'à ce que la route touche le bout de l'espace
                while x < 25 and y < 25 and x >= 0 and y >= 0 and j < distance:

                    # S'il n'y a pas déjà de route : crée une route
                    if routes[y][x] == 0:
                        routes[y][x] = Espace(x, y, 1, direction, "non")

                    # S'il y a déjà une route, transforme la route existante en intersection
                    else:
                        routes[y][x].type = 2
                        routes[y][x].direction = "non"
                        routes[y][x].type_inter = "priorité à droite"

                    # chgt_direction=rd.random()
                    # if chgt_direction<0.1 and (direction=="haut" or direction=="bas"):
                    #     direction=rd.choice(["gauche","droite"])
                    # elif chgt_direction<0.1 and (direction=="gauche" or direction=="droite"):
                    #     direction=rd.choice(["haut","bas"])

                    # On se déplace selon la direction
                    if direction == "haut":
                        y -= 1
                    elif direction == "bas":
                        y += 1
                    elif direction == "gauche":
                        x -= 1
                    elif direction == "droite":
                        x += 1

                    j += 1

                # Ajout de la fin de la route : dépend de la direction
                if direction == "haut":
                    fin_routes.append((x, y + 1))
                elif direction == "bas":
                    fin_routes.append((x, y - 1))
                elif direction == "gauche":
                    fin_routes.append((x + 1, y))
                elif direction == "droite":
                    fin_routes.append((x - 1, y))
            elif y==24 and checkboxes_variables[y][x].get():
                direction='haut'

                # Ajout du point de départ de la route
                depart_routes.append((x, y))

                # Ajout d'une distance maximale pour ne pas avoir de boucle infinie
                distance = 100 * 25
                j = 0

                # Boucle jusqu'à ce que la route touche le bout de l'espace
                while x < 25 and y < 25 and x >= 0 and y >= 0 and j < distance:

                    # S'il n'y a pas déjà de route : crée une route
                    if routes[y][x] == 0:
                        routes[y][x] = Espace(x, y, 1, direction, "non")

                    # S'il y a déjà une route, transforme la route existante en intersection
                    else:
                        routes[y][x].type = 2
                        routes[y][x].direction = "non"
                        routes[y][x].type_inter = "priorité à droite"

                    # chgt_direction=rd.random()
                    # if chgt_direction<0.1 and (direction=="haut" or direction=="bas"):
                    #     direction=rd.choice(["gauche","droite"])
                    # elif chgt_direction<0.1 and (direction=="gauche" or direction=="droite"):
                    #     direction=rd.choice(["haut","bas"])

                    # On se déplace selon la direction
                    if direction == "haut":
                        y -= 1
                    elif direction == "bas":
                        y += 1
                    elif direction == "gauche":
                        x -= 1
                    elif direction == "droite":
                        x += 1

                    j += 1

                # Ajout de la fin de la route : dépend de la direction
                if direction == "haut":
                    fin_routes.append((x, y + 1))
                elif direction == "bas":
                    fin_routes.append((x, y - 1))
                elif direction == "gauche":
                    fin_routes.append((x + 1, y))
                elif direction == "droite":
                    fin_routes.append((x - 1, y))
    for y in range(1,24):
        for x in [0,24]:
            if x==0 and checkboxes_variables[y][x].get():
                direction='droite'

                # Ajout du point de départ de la route
                depart_routes.append((x, y))

                # Ajout d'une distance maximale pour ne pas avoir de boucle infinie
                distance = 100 * 25
                j = 0

                # Boucle jusqu'à ce que la route touche le bout de l'espace
                while x < 25 and y < 25 and x >= 0 and y >= 0 and j < distance:

                    # S'il n'y a pas déjà de route : crée une route
                    if routes[y][x] == 0:
                        routes[y][x] = Espace(x, y, 1, direction, "non")

                    # S'il y a déjà une route, transforme la route existante en intersection
                    else:
                        routes[y][x].type = 2
                        routes[y][x].direction = "non"
                        routes[y][x].type_inter = "priorité à droite"

                    # chgt_direction=rd.random()
                    # if chgt_direction<0.1 and (direction=="haut" or direction=="bas"):
                    #     direction=rd.choice(["gauche","droite"])
                    # elif chgt_direction<0.1 and (direction=="gauche" or direction=="droite"):
                    #     direction=rd.choice(["haut","bas"])

                    # On se déplace selon la direction
                    if direction == "haut":
                        y -= 1
                    elif direction == "bas":
                        y += 1
                    elif direction == "gauche":
                        x -= 1
                    elif direction == "droite":
                        x += 1

                    j += 1

                # Ajout de la fin de la route : dépend de la direction
                if direction == "haut":
                    fin_routes.append((x, y + 1))
                elif direction == "bas":
                    fin_routes.append((x, y - 1))
                elif direction == "gauche":
                    fin_routes.append((x + 1, y))
                elif direction == "droite":
                    fin_routes.append((x - 1, y))
            elif x==24 and checkboxes_variables[y][x].get():
                direction='gauche'

                # Ajout du point de départ de la route
                depart_routes.append((x, y))

                # Ajout d'une distance maximale pour ne pas avoir de boucle infinie
                distance = 100 * 25
                j = 0

                # Boucle jusqu'à ce que la route touche le bout de l'espace
                while x < 25 and y < 25 and x >= 0 and y >= 0 and j < distance:

                    # S'il n'y a pas déjà de route : crée une route
                    if routes[y][x] == 0:
                        routes[y][x] = Espace(x, y, 1, direction, "non")

                    # S'il y a déjà une route, transforme la route existante en intersection
                    else:
                        routes[y][x].type = 2
                        routes[y][x].direction = "non"
                        routes[y][x].type_inter = "priorité à droite"

                    # chgt_direction=rd.random()
                    # if chgt_direction<0.1 and (direction=="haut" or direction=="bas"):
                    #     direction=rd.choice(["gauche","droite"])
                    # elif chgt_direction<0.1 and (direction=="gauche" or direction=="droite"):
                    #     direction=rd.choice(["haut","bas"])

                    # On se déplace selon la direction
                    if direction == "haut":
                        y -= 1
                    elif direction == "bas":
                        y += 1
                    elif direction == "gauche":
                        x -= 1
                    elif direction == "droite":
                        x += 1

                    j += 1

                # Ajout de la fin de la route : dépend de la direction
                if direction == "haut":
                    fin_routes.append((x, y + 1))
                elif direction == "bas":
                    fin_routes.append((x, y - 1))
                elif direction == "gauche":
                    fin_routes.append((x + 1, y))
                elif direction == "droite":
                    fin_routes.append((x - 1, y))
    for x in range(25):
        for y in range(25):
            if routes[y][x] == 0:
                routes[y][x] = Espace(x, y, 0, "non", "non")

    main_frame_1.grid_remove()

    for x in range(25):
        for y in range(25):
            if routes[y][x]!=0:
                if routes[y][x].type==1:
                    checkboxes_2[y][x]=tk.Checkbutton(subframe_4,state='disabled')
                    checkboxes_2[y][x].grid(row=y,column=x)
                elif routes[y][x].type==2:
                    checkboxes_variables_2[y][x]=tk.BooleanVar(subframe_4,"0")
                    checkboxes_2[y][x]=tk.Checkbutton(subframe_4,variable=checkboxes_variables_2[y][x],bg='white',activebackground='white')
                    checkboxes_2[y][x].grid(row=y,column=x)
    subframe_4.grid(row=1,column=0)
    main_frame_2.grid(row=0,column=0)

def intersection_type():
    global window,main_frame_2,main_frame_3,densite,routes,depart_routes,fin_routes,checkboxes_variables_2
    for x in range(25):
        for y in range(25):
            if checkboxes_variables_2[y][x]!=0 and checkboxes_variables_2[y][x].get():
                routes[y][x].type_inter="feu rouge"

    main_frame_2.grid_remove()
    main_frame_3.grid(row=0,column=0)

def lancer_animation():
    global routes,depart_routes,fin_routes,densite,position_routes, position_voitures,n,colormap
    position_routes=routes
    proba_apparition=densite.get()
    n=0
    position_voitures = [[0 for _ in range(25)] for _ in range(25)]
    generer_voiture.generer_voiture(position_voitures, depart_routes, position_routes, proba_apparition)
    # Création de la colormap personnalisée
    colormap=LinearSegmentedColormap.from_list('CustomColormap',[(1,1,1),(0,1,0),(1,0,0),(0,0,0)])

    # Création de la figure et de la première image de l'animation
    fig = plt.figure()
    image_array=generation_image(position_routes, position_voitures)
    im = plt.imshow(image_array, cmap=colormap, animated=True)


    # Définition de la fonction d'animation
    def updatefig(*args):
        global position_voitures, position_routes, depart_routes, fin_routes, n,colormap

        position_voitures = next_step(position_routes, position_voitures, depart_routes, fin_routes, n,proba_apparition)
        im.set_array(generation_image(position_routes, position_voitures))

        # Itération suivante
        n += 1
        return im,


    ani = animation.FuncAnimation(fig, updatefig, interval=300, blit=True,repeat=False)
    plt.axis('off')
    plt.show()

window = tk.Tk()
window.title("Génération univers")
window.resizable(False,False)

#fenêtre 1
main_frame_1=tk.Frame(window,bg='white')

subframe_2=tk.Frame(main_frame_1,bg='white')
label_1=tk.Label(subframe_2,bg='white',text="Entrez le point de départ des routes",width=30)
label_1.grid(row=0,column=0)
label_empty=tk.Label(subframe_2,bg='white',width=15)
label_empty.grid(row=0,column=1)
button_1=tk.Button(subframe_2,bg='white',text="Valider",width=30,command=generate_universe_from_gui)
button_1.grid(row=0,column=2)
subframe_2.grid(row=0,column=0)

subframe_1=tk.Frame(main_frame_1,bg='white')

checkboxes=[[0 for _ in range(25)] for _ in range(25)]
checkboxes_variables=[[0 for _ in range(25)] for _ in range(25)]

for i in range(1,24):
    for j in [0,24]:
        checkboxes_variables[j][i]=tk.BooleanVar(subframe_1,"0")
        checkboxes[j][i]=tk.Checkbutton(subframe_1,variable=checkboxes_variables[j][i],bg='white',activebackground='white')
        checkboxes[j][i].grid(row=j,column=i)
for j in range(1,24):
    for i in [0,24]:
        checkboxes_variables[j][i]=tk.BooleanVar(subframe_1,"0")
        checkboxes[j][i]=tk.Checkbutton(subframe_1,variable=checkboxes_variables[j][i],bg='white',activebackground='white')
        checkboxes[j][i].grid(row=j,column=i)

subframe_1.grid(row=1,column=0)

main_frame_1.grid(row=0,column=0)

#fenêtre 2
main_frame_2=tk.Frame(window,bg='white')

subframe_3=tk.Frame(main_frame_2,bg='white')
label_2=tk.Label(subframe_3,bg='white',text="Entrez le type d'intersection (feu rouge si coché, priorité à droite sinon)")
label_2.grid(row=0,column=0)
button_2=tk.Button(subframe_3,bg='white',text='Valider',width=30,command=intersection_type)
button_2.grid(row=0,column=1)
subframe_3.grid(row=0,column=0)

subframe_4=tk.Frame(main_frame_2,bg='white')
checkboxes_2=[[0 for _ in range(25)] for _ in range(25)]
checkboxes_variables_2=[[0 for _ in range(25)] for _ in range(25)]

main_frame_3=tk.Frame(window,bg='white')
label_3=tk.Label(main_frame_3,bg='white',text='Densité du traffic')
label_3.grid(row=0,column=0)

densite=tk.DoubleVar(main_frame_3)
densite.set(0.1)
scalar=tk.Scale(main_frame_3,variable=densite,orient='horizontal',from_=0,to=1,resolution=0.025,length=350)
scalar.grid(row=1,column=0)

button_3=tk.Button(main_frame_3,bg='white',text='Lancer la simulation',width=30,command=lancer_animation)
button_3.grid(row=2,column=0)





window.mainloop()



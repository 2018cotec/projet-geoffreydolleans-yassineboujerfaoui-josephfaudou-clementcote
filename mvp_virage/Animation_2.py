import matplotlib.pyplot as plt
import matplotlib.animation as animation
import generer_voiture
import Remonte_virage_2
import Avance_virage_2
import Espace2
import pickle
from matplotlib.colors import LinearSegmentedColormap


def generation_image(position_routes, position_voitures):
    """
    :param position_routes: liste de liste contenant des objets de type espace, donnant la position des routes et des intersections
    :param position_voitures: liste de liste contenant des objets de type voiture ou des espaces
    :return: un tableau "image" des routes et des voitures, interprétable par plt.imshow
    """

    # Récupération de la taille de l'univers
    taille = len(position_routes)

    # Création du tableau image
    image = [[0 for i in range(10 * taille)] for j in range(10 * taille)]

    # Parcours des tableaux position_routes et position_images
    for i in range(taille):
        for j in range(taille):

            # S'il y a une route, on dessine un carré
            if position_routes[j][i].type == 1:
                for k in range(10):
                    image[10 * j][10 * i + k] = 3
                    image[10 * j + k][10 * i] = 3
                    image[10 * j + 9][10 * i + k] = 3
                    image[10 * j + k][10 * i + 9] = 3

            # S'il y a une intersection
            elif position_routes[j][i].type == 2:
                # Si c'est une priorité à droite on affiche en vert
                if position_routes[j][i].type_inter=="priorité à droite":
                    for k in range(10):
                        image[10 * j][10 * i + k] = 1
                        image[10 * j + k][10 * i] = 1
                        image[10 * j + 9][10 * i + k] = 1
                        image[10 * j + k][10 * i + 9] = 1
                # Sinon (un feu rouge) on affiche en rouge
                else:
                    for k in range(10):
                        image[10 * j][10 * i + k] = 2
                        image[10 * j + k][10 * i] = 2
                        image[10 * j + 9][10 * i + k] = 2
                        image[10 * j + k][10 * i + 9] = 2

            # S'il y a une voiture, on remplit le carré
            if position_voitures[j][i] != 0:
                for k in range(1,9):
                    for p in range(1,9):
                        image[10 * j + p][10 * i + k] = 3
    return image


def next_step(position_routes, position_voitures, depart_routes, fin_routes, n):
    """
    :param position_routes: liste de liste contenant des objets de type espace, donnant la position des routes et des intersections
    :param position_voitures: liste de liste contenant des objets de type voiture ou des espaces
    :param depart_routes: liste de tuples contenant les positions de départ des routes
    :param fin_routes: liste de tuples contenant les positions de fin des routes
    :param n: numéro de l'itération
    :return: le tableau position_voitures à l'itération suivante
    """

    # On génère les voitures qui apparaissent au début des routes
    generer_voiture.generer_voiture(position_voitures, depart_routes, position_routes, 0.1)

    # On régle la vitesse de toutes les voitures
    Remonte_virage_2.remonte_route(fin_routes, depart_routes, position_routes, position_voitures, n)

    # On fait avancer toutes les voitures selon leur vitesses
    position_voitures = Avance_virage_2.avancer_voitures(position_voitures, position_routes)

    return position_voitures


if __name__ == '__main__':
    # Initialisation des variables
    n = 0
    position_routes, depart_routes, fin_routes = Espace2.generer_espace(50, 6)
    position_voitures = [[0 for _ in range(50)] for _ in range(50)]
    generer_voiture.generer_voiture(position_voitures, depart_routes, position_routes, 0.3)
    #
    # with open('univers.save','wb') as f:
    #     my_pickler=pickle.Pickler(f)
    #     my_pickler.dump(position_routes)
    #
    # with open('depart.save','wb') as f:
    #     my_pickler=pickle.Pickler(f)
    #     my_pickler.dump(depart_routes)
    #
    # with open('fin.save','wb') as f:
    #     my_pickler=pickle.Pickler(f)
    #     my_pickler.dump(fin_routes)
    #
    # with open('voitures.save','wb') as f:
    #     my_pickler=pickle.Pickler(f)
    #     my_pickler.dump(position_voitures)

    # with open('univers.save', 'rb') as f:
    #     my_depickler = pickle.Unpickler(f)
    #     position_routes = my_depickler.load()
    #
    # with open('voitures.save', 'rb') as f:
    #     my_depickler = pickle.Unpickler(f)
    #     position_voitures = my_depickler.load()
    #
    # with open('depart.save', 'rb') as f:
    #     my_depickler = pickle.Unpickler(f)
    #     depart_routes = my_depickler.load()
    #
    # with open('fin.save', 'rb') as f:
    #     my_depickler = pickle.Unpickler(f)
    #     fin_routes = my_depickler.load()

    # Création de la colormap personnalisée
    colormap=LinearSegmentedColormap.from_list('CustomColormap',[(1,1,1),(0,1,0),(1,0,0),(0,0,0)])

    # Création de la figure et de la première image de l'animation
    fig = plt.figure()
    im = plt.imshow(generation_image(position_routes, position_voitures), cmap=colormap, animated=True)


    # Définition de la fonction d'animation
    def updatefig(*args):
        global position_voitures, position_routes, depart_routes, fin_routes, n

        position_voitures = next_step(position_routes, position_voitures, depart_routes, fin_routes, n)
        im.set_array(generation_image(position_routes, position_voitures))

        # Itération suivante
        n += 1
        return im,


    ani = animation.FuncAnimation(fig, updatefig, interval=300, blit=True,repeat=False)
    plt.show()

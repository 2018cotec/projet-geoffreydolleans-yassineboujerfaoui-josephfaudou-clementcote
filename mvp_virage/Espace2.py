import random as rd

class Espace:
    """Classe qui définit une case d'espace caractérisée par :
    - sa position dans l'espace
    - son type (vide (0), route (1) ou intersection (2))
    - son type d'intersection ("non", "feu_rouge", "priorité_à_droite")
    - sa limitation de vitesse
    - la probabilité qu'une voiture apparaisse sur cette case (au bord de l'espace)
    """

    def __init__(self, x, y, type, direction, type_inter, v_lim = 1, p_arrivee = 0, virage = None):
        self.position = (x, y)
        self.type = type
        self.type_inter = type_inter
        self.direction = direction
        self.v_lim = v_lim
        self.p_arrivee = p_arrivee
        self.virage = virage


def generer_espace(taille, nombre_route):
    """
    :param taille: taille de l'espace que l'on va générer
    :param nombre_route: nombre de routes qui sera généré
    :return: routes : tableau de tableau contenant des objets espaces, depart_route : liste de tuples contenant les positions de départ des routes, fin_route : liste de tuples contenant les positions de fin des routes
    """

    # Génération du tableau
    routes = [[0 for i in range(taille)] for j in range(taille)]

    # Tableaux contenants respectivement les valeurs de départ sur x et sur y des routes
    x_start = []
    y_start = []

    # Tableaux contenants respectivement les coordonnées de départ et d'arrivée des routes
    depart_route = []
    fin_route = []
    # Tableaux contenant l'ordonné ou l'abscisse des virages pour éviter que deux routes se mélangent
    liste_x_virage, liste_y_virage = [], []

    for i in range(nombre_route):
        # Choix de la direction de la route
        depart = rd.choice(["x", "y"])

        if depart == 'x':
            # Choix du sens de la route
            x = rd.choice([0, taille - 1])

            # Choix de l'ordonnée de départ, et vérification qu'elle n'est pas adjacente à une route
            y = rd.randint(1, taille - 2)
            while y + 1 in y_start or y - 1 in y_start or y in y_start or y in liste_y_virage:
                y = rd.randint(1, taille - 2)

            # Ajout de la position de départ dans y_start
            y_start.append(y)

            # Fixe la valeur de la direction
            if x == 0:
                direction = "droite"
            else:
                direction = "gauche"

        # Equivalent au if précédent
        else:
            y = rd.choice([0, taille - 1])
            x = rd.randint(1, taille - 2)
            while x + 1 in x_start or x - 1 in x_start or x in x_start or x in liste_x_virage:
                x = rd.randint(1, taille - 2)
            x_start.append(x)
            if y == 0:
                direction = "bas"
            else:
                direction = "haut"

        # Ajout du point de départ de la route
        depart_route.append((x, y))

        # Ajout d'une distance maximale pour ne pas avoir de boucle infinie
        distance = 100 * taille
        j = 0
        k = 0  #compteur de virage
        # Boucle jusqu'à ce que la route touche le bout de l'espace
        while x < taille and y < taille and x >= 0 and y >= 0 and j < distance:

            # S'il n'y a pas déjà de route : crée une route
            if routes[y][x] == 0:
                routes[y][x] = Espace(x, y, 1, direction, "non")

            # S'il y a déjà une route, transforme la route existante en intersection
            else:
                routes[y][x].type = 2
                routes[y][x].direction = "non"
                if rd.random() < 1/2:
                    routes[y][x].type_inter = "priorité à droite"
                else:
                    routes[y][x].type_inter = "feu rouge"

            def virage_gauche(direction) :
                 if direction == "haut" :
                      return "gauche"
                 elif direction == "bas" :
                      return "droite"
                 elif direction == "gauche" :
                      return "bas"
                 elif direction == "droite" :
                      return "haut"

            def virage_droit(direction) :
                 if direction == "haut" :
                      return "droite"
                 elif direction == "bas" :
                      return "gauche"
                 elif direction == "gauche" :
                      return "haut"
                 elif direction == "droite" :
                      return "bas"

            def direction_apres(direction_avant, virage):
                 if virage == "droite":
                      return virage_droit(direction_avant)
                 elif virage == "gauche":
                      return virage_gauche(direction_avant)

            def direction_deplacement(direction):
                if direction == "haut" :
                    return (0,-1)
                elif direction == "gauche" :
                    return (-1,0)
                elif direction == "droite" :
                    return (1,0)
                elif direction == "bas" :
                    return (0,1)

            direc_dep = direction_deplacement(direction)
            if routes[y][x].type != 2:
                #pas de virage a une intersection
                a, b = x + direc_dep[0], y + direc_dep[1]
                c, d = x - direc_dep[0], y - direc_dep[1]
                if 0<= a<= taille - 1 and 0 <= b <= taille - 1 and 0<= c<= taille - 1 and 0 <= d <= taille - 1 and routes[b][a] == 0 and routes[d][c] !=2 :
                    #Si la route pourrait se mélanger ou se coller a une autre route on ne tourne pas
                    if x not in liste_x_virage and y not in liste_y_virage:
                        #Traite le cas ou la route est initiallement horizontale
                        if direction in ["droite", "gauche"]:
                            if x not in x_start and x + 1 not in x_start and x - 1 not in x_start:
                                if k<1 and x != 0 and x != taille - 1 and y!=0 and y!= taille -1 :
                                    changement_direction = rd.random()
                                    if changement_direction < .05 :
                                        virage = rd.choice(["gauche","droite"])
                                        direction = direction_apres(direction, virage)
                                        #on doit retenir l'ordonné du virage et ses voisins pour eviter aussi deux routes collées
                                        liste_y_virage.append(y-1)
                                        liste_y_virage.append(y)
                                        liste_y_virage.append(y+1)
                                        liste_x_virage.append(x-1)
                                        liste_x_virage.append(x)
                                        liste_x_virage.append(x+1)
                                                
                                            
                                        routes[y][x].direction = direction
                                        routes[y][x].virage = virage
                                        k+=1
                        #Traite le cas ou la route est verticale
                        else    :
                            if y not in y_start and y + 1 not in y_start and y - 1 not in y_start:
                                if k<1 and x != 0 and x != taille - 1 and y!=0 and y!= taille -1 :
                                    changement_direction = rd.random()
                                    if changement_direction < .05 :
                                        virage = rd.choice(["gauche","droite"])
                                        direction = direction_apres(direction, virage)
                                        #on doit retenir l'ordonné du virage et ses voisins pour eviter aussi deux routes collées
                                        liste_y_virage.append(y-1)
                                        liste_y_virage.append(y)
                                        liste_y_virage.append(y+1)
                                        liste_x_virage.append(x-1)
                                        liste_x_virage.append(x)
                                        liste_x_virage.append(x+1)
                                                
                                            
                                        routes[y][x].direction = direction
                                        routes[y][x].virage = virage
                                        k+=1


            if direction == "haut":
                    y -= 1
            elif direction == "bas":
                    y += 1
            elif direction == "gauche":
                    x -= 1
            elif direction == "droite":
                    x += 1
        
            j += 1

        # Ajout de la fin de la route : dépend de la direction
        if direction == "haut":
            fin_route.append((x, y + 1))
        elif direction == "bas":
            fin_route.append((x, y - 1))
        elif direction == "gauche":
            fin_route.append((x + 1, y))
        elif direction == "droite":
            fin_route.append((x - 1, y))

        # On remplit les cases qui ne sont pas des routes avec des espaces de type 0
    for x in range(taille):
        for y in range(taille):
            if routes[y][x] == 0:
                routes[y][x] = Espace(x, y, 0, "non", "non")
    return routes, depart_route, fin_route

if __name__=="__main__":
    import matplotlib.pyplot as plt
    routes,debut_route,fin_route=generer_espace(50,20)
    affichage=[[0 for _ in range(50)] for _ in range(50)]
    for i in range(50):
        for j in range(50):
            affichage[i][j]=routes[i][j].type
    plt.imshow(affichage,cmap='Greys')
    plt.show()

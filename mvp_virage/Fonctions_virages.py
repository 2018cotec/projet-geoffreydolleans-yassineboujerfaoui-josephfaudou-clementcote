import random as rd
from Espace2 import *
from Classes import *
from Test import *

def direction_deplacement(espace) :
    """Fonction qui retourne le couple de déplacement en fonction de la direction de la route"""
    if espace.direction == "haut" :
        return (0,-1)
    if espace.direction == "gauche" :
        return (-1,0)
    if espace.direction == "droite" :
        return (1,0)
    if espace.direction == "bas" :
        return (0,1)

def voiture_suivante(voiture,position_voitures,position_routes) :
    """:param voiture : Classe voiture pour laquelle on va retrouver la voiture qui la suit
    :param position_voitures : Liste de listes ayant comme éléments les objets voitures
    :param position_routes : Liste de listes ayant comme éléments les objets routes
    :return Soit None , soit une chaine de caractère qui signale la présence d'une intersection devant la voiture avec le nombre de cases entre la voiture
    et l'intersection et l'objet route qui se trouve à l'intersection"""
    x,y = voiture.position
    i = 0 #Initialisation du compteur de nombre de cases séparant la voiture et la voiture qui la suit
    while i < voiture.v :
        try:

            #On gere le cas ou la voiture est sur l'intersection
            if position_routes[y][x].type == 2:
                dir = voiture.direction
                if dir == "haut":
                    y -= 1
                elif dir == "bas":
                    y += 1
                elif dir == "droite":
                    x += 1
                else:
                    x -= 1
            else:
                x, y = x + direction_deplacement(position_routes[y][x])[0], y + direction_deplacement(position_routes[y][x])[1]
            #Si la route est de type intersection , on arrête la voiture juste avant l'intersection
            if position_routes[y][x].type == 2 :
                return "intersection",i
            #Si la route contient une voiture suivante , on retourne la voiture qui la suit avec le nombre de cases les séparants
            if position_voitures[y][x] != 0 :
                return "voiture_suiv",i ,position_voitures[y][x]
            i += 1
                
          
        except IndexError:
            #Si on arrive en dehors de la grille on ne renvoie rien
            return None
                
        
            
    
    #S'il n'y a ni voiture ni intersection , on ne retourne rien        
    return None

def test_virage(voiture,position_routes):
    """la fonction teste si la voiture va tourner"""
    x,y = voiture.position
    i = 0 #Initialisation du compteur de nombre de cases séparant la voiture et la voiture qui la suit
    while i < voiture.v :
        try:

            #On gere le cas ou la voiture est sur l'intersection
            if position_routes[y][x].type == 2:
                dir = voiture.direction
                if dir == "haut":
                    y -= 1
                elif dir == "bas":
                    y += 1
                elif dir == "droite":
                    x += 1
                else:
                    x -= 1
            else:
                x, y = x + direction_deplacement(position_routes[y][x])[0], y + direction_deplacement(position_routes[y][x])[1]
            #Si la route a un virage , on retourne True et la voiture freinera
            if position_routes[y][x].virage != None :
                return True
            i += 1
            
        except IndexError:
            
            #Si on arrive en dehors de la grille on renvoie False
            return False
            
    #Si elle a fait tout son chemin sans virage on renvoie False
    return False

def ajustement(voiture,position_voitures,position_routes) :
    """Fonction qui ajuste la vitesse de la voiture"""
    voit_suiv = voiture_suivante(voiture,position_voitures,position_routes) #On récupère la voiture ou l'intersection qui suit notre voiture
    if voit_suiv != None :
        #On distingue les deux cas où on a une voiture ou une intersection
        if voit_suiv[0] == "intersection" :
            voiture.v = voit_suiv[1]
        elif  voiture.v - voit_suiv[-1].v > voit_suiv[1] :
            #Si la voiture va rattraper la voiture suivante alors elle ralentit et copie sa vitesse
            voiture.v = voit_suiv[-1].v
    else:
        #on traite le cas ou la voiture rencontre un virage et doit ralentire
        if test_virage(voiture,position_routes):
            if voiture.v !=1:
                voiture.v -= 1
        
        #On traite le cas sans virage ou la voiture peut accelerer et freiner
        else:
            if voiture.v !=0:
                if rd.random() < voiture.proba_freinage:
                    voiture.v -= 1
                elif voiture.v < position_routes[voiture.position[1]][voiture.position[0]].v_lim :
                    #La voiture accelère si elle est inférieur a la vitesse limite a moins qu'elle rencontre un problème si elle accélère
                    voiture.v += 1
                    if voiture_suivante(voiture, position_voitures, position_routes) != None:
                        voiture.v -= 1
            else:
                if voiture.v < position_routes[voiture.position[1]][voiture.position[0]].v_lim :
                    #La voiture accelère a moins qu'elle rencontre un problème si elle accélère
                    voiture.v += 1
                    if voiture_suivante(voiture, position_voitures, position_routes) != None:
                        voiture.v -= 1





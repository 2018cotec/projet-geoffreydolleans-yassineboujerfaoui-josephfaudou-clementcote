# MVP Virage et Vitesse
Exécuter le fichier main.py

# Que fait ce programme ?
Il simule la même chose que le MVP, avec l'ajout de 2 fonctionnalités :
- Les routes peuvent désormais avoir des virages
- Les voitures peuvent désormais avoir des vitesse supérieures à 1 case par itération

# Dépendances
- Matplotlib

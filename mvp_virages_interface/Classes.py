class voiture :
    """Classe qui définit une voiture caractérisée par :
    - sa position dans l'espace
    - sa vitesse
    - la probabilité qu'elle freine
    - sa direction (définie par sa position sur une route)
    """
    def __init__(self,pos,v,position_routes,proba_freinage=0.1):
        self.position=pos
        self.v=v
        self.proba_freinage=proba_freinage
        self.direction=position_routes[pos[1]][pos[0]].direction

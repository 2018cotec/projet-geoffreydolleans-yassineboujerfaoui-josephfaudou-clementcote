# MVP Simulation avec virage, vitesse, freinage et interface
Exécuter le fichier main.py

# Que fait ce programme ?
Il permet de choisir les paramètres de la simulation :
    - taille de l'univers
    - nombre de routes
    - vitesse limite
    - type d'intersection
    - densité de circulation

De plus :
    - Les routes peuvent avoir des virages
    - Les voitures peuvent accélérer et freiner

# Dépendances
- Matplotlib
- tkinter

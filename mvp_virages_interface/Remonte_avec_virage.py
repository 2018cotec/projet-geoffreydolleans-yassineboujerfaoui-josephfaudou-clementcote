from intersection import *
from Fonctions import *
from main import *


def direction_remonter(espace, dir_nom=None):
    """renvoie un couple x,y qui permet d'acceder à la case de la route précedente"""
    if espace.virage == "droite":
        if espace.direction == "haut":
            return +1, 0
        elif espace.direction == "bas":
            return -1, 0
        elif espace.direction == "droite":
            return 0, +1
        elif espace.direction == "gauche":
            return 0, -1
    elif espace.virage == "gauche":
        #On prend la route dans le mauvais sens et on trouve la direction avant le virage
        if espace.direction == "haut":
            #on va vers la gauche
            return -1, 0
        elif espace.direction == "bas":
            #on va vers la droite
            return +1, 0
        elif espace.direction == "droite":
            #on va vers le haut
            return 0, -1
        else:
            return 0, +1
    else:
        #Il n'y a pas de virage
        if espace.direction == "haut":
            return 0, +1
        elif espace.direction == "bas":
            return 0, -1
        elif espace.direction == "droite":
            return -1, 0
        elif espace.direction == "gauche":
            return +1, 0
        
        
        

def Changement_vitesse(voiture, position_voitures, position_routes,n):
    """ajuste la vitesse de la voiture pour eviter toute collision, s'arreter avant les intersections et gere tout type d'intersection"""
    x,y = voiture.position
    
    #si on est sur une intersection, on ajuste la vitesse de la voiture
    if position_routes[x][y] == 2:
        ajustement(voiture, position_voitures, position_routes)
    else:
        #on va voir la prochaine case
        direction = voiture.direction
        if direction == "haut":
            y -=1
        elif direction == "bas":
            y +=1
        elif direction == "droite":
            x += 1
        elif direction == "gauche":
            x -= 1
        try:
            #si la prochaine case est un intersection appelle la f° intersection
            if position_routes[y][x].type == 2:
                intersection(x, y,position_routes,position_voitures,n)
            #sinon verifie si la voiture doit accelerer ou freiner
            else:
                ajustement(voiture, position_voitures, position_routes)
        except IndexError:
            pass


def remonte_route(fin_route, depart_route, position_routes, position_voitures, n):
    """parcourt toutes les routes en sens inverse pour ajuster la vitesse de toutes les voitures dans le bon ordre"""
    for pos in fin_route:
        x, y = pos[0], pos[1]
        dir_nom = position_routes[y][x].direction
        direction = direction_remonter(position_routes[y][x], dir_nom)
        x, y = x + direction[0], y + direction[1]
        while (x,y) not in depart_route:
            #si il y a une intersection, on continue tout droit
            if position_routes[y][x].type !=2:
                direction = direction_remonter(position_routes[y][x], dir_nom)
                dir_nom = position_routes[y][x].direction
            x, y = x + direction[0], y + direction[1]
            #si on trouve une voiture on ajuste sa vitesse
            if position_voitures[y][x] != 0:
                Changement_vitesse(position_voitures[y][x],position_voitures,position_routes,n)

if __name__ == "__main__":
    #teste la fonction remonte route
    routes = [[Espace(0,0, 0, "non", "non"), Espace(1,0, 0, "non", "non"), Espace(2, 0, 0, "non", "non")],[Espace(0, 1, 1, "droite", "non"), Espace(1, 1, 1, "bas", "non"),Espace(2, 1, 0, "non", "non")],[Espace(0,2, 0, "non", "non"), Espace(1,2, 1, "bas", "non"), Espace(2,2, 0, "non", "non")]]
    routes[1][1].virage = "droite"
    voitures = [[0,0,0],[0,0,0],[0,0,0]]
    depart_route = [(0,1)]
    fin_route = [(1,2)]
    remonte_route(fin_route, depart_route, routes, voitures, 1)

import random as rd
from Classes import *
from Fonctions import *
from main import *

def avancer_voitures(position_voitures, position_routes):
    """
    :param position_voitures: liste des positions des voitures
    :param position_routes: liste des positions des routes
    :return: liste des voitures avec leur nouvelle position
    """
    taille = len(position_voitures)
    new_position_voiture = [[0 for _ in range(taille)] for _ in range (taille)] #créer un univers vide
    for ligne in position_voitures:
        for voiture in ligne:
            i = 0
            if voiture != 0:
                while i < voiture.v:
                    x, y = voiture.position
                    direction = voiture.direction
                    if direction == 'haut': #modifie la position de la voiture en fonction de sa direction et de sa vitesse
                        y -= 1
                    elif direction == 'bas':
                        y += 1
                    elif direction =='droite':
                        x += 1
                    else:
                        x -= 1
                    voiture.position = (x, y)
                    #il faut trouver la direction de la voiture
                    if 0 <= x <= taille - 1 and 0 <= y <= taille - 1 and  position_routes[y][x].type != 2:
                        voiture.direction = position_routes[y][x].direction
                    i += 1


                if 0 <= voiture.position[0] <= taille-1 and 0 <= voiture.position[1] <= taille-1 :
                    new_position_voiture[voiture.position[1]][voiture.position[0]]=voiture #ajoute la voiture au nouvel univers si elle est dedans

    return new_position_voiture



def test_freinage_dans_virage():
    #teste la fonction ajustement si il y a des virages, la voiture freine
    routes = [[Espace(0,0, 0, "non", "non"), Espace(1,0, 0, "non", "non"), Espace(2, 0, 0, "non", "non")],[Espace(0, 1, 1, "droite", "non"), Espace(1, 1, 1, "bas", "non"),Espace(2, 1, 0, "non", "non")],[Espace(0,2, 0, "non", "non"), Espace(1,2, 1, "bas", "non"), Espace(2,2, 0, "non", "non")]]
    routes[1][1].virage = "droite"
    v = rd.randint(2,6)
    voitures = [[0 ,0,0],[voiture((0,1),v,routes),0,0],[0,0,0]]
    depart_route = [(0,1)]
    fin_route = [(1,2)]
    ajustement(voitures[1][0], voitures, routes)
    assert voitures[1][0].v == v-1

def test_freinage_ligne_droite():
    #teste la fonction ajustement en ligne droite avec une probabilité de freinage de 1
    routes = [[Espace(0, 0, 0, "non", "non"), Espace(1, 0, 0, "non", "non"), Espace(2, 0, 0, "non", "non")],[Espace(0, 1, 1, "droite", "non"), Espace(1, 1, 1, "droite", "non"),Espace(2, 1, 0, "droite", "non")],[Espace(0,2, 0, "non", "non"), Espace(1,2, 0, "non", "non"), Espace(2,2, 0, "non", "non")]]
    v = rd.randint(2,6)
    voitures = [[0 ,0,0],[voiture((0,1),v,routes,1),0,0],[0,0,0]]
    depart_route = [(0,1)]
    fin_route = [(1,2)]
    ajustement(voitures[1][0], voitures, routes)
    assert voitures[1][0].v == v-1

def test_arret_avt_inter():
    #teste que la fonction s'arrete avant l'intersection
    routes = [[Espace(0,0, 0, "non", "non"), Espace(1,0, 0, "non", "non"), Espace(2, 0, 0, "non", "non")],[Espace(0, 1, 1, "droite", "non"), Espace(1, 1, 1, "droite", "non"),Espace(2, 1, 2, "non", "feu rouge")],[Espace(0,2, 0, "non", "non"), Espace(1,2, 0, "non", "non"), Espace(2,2, 0, "non", "non")]]
    v = rd.randint(2,6)
    voitures = [[0 ,0 , 0],[voiture((0,1), v, routes, 1), 0, 0],[0, 0, 0]]
    depart_route = [(0, 1)]
    fin_route = [(1, 2)]
    ajustement(voitures[1][0], voitures, routes)
    assert voitures[1][0].v == 1
    
def test_acc():
    #teste la fonction ajustement en ligne droite avec une probabilité de freinage nulle, la voiture doit accéler
    routes = [[Espace(0, 0, 0, "non", "non"), Espace(1, 0, 0, "non", "non"), Espace(2, 0, 0, "non", "non")],[Espace(0, 1, 1, "droite", "non",10), Espace(1, 1, 1, "droite", "non", 10),Espace(2, 1, 0, "droite", "non", 10)],[Espace(0, 2, 0, "non", "non"), Espace(1, 2, 0, "non", "non"), Espace(2, 2, 0, "non", "non")]]
    v = rd.randint(2, 6)
    voitures = [[0 ,0 ,0],[voiture((0, 1), v, routes, 0), 0, 0],[0, 0, 0]]
    depart_route = [(0, 1)]
    fin_route = [(1, 2)]
    ajustement(voitures[1][0], voitures, routes)
    assert voitures[1][0].v == v+1
    
def test_v_voiture_devant():
    #teste la fonction ajustement si il y a une voiture devant et que la voiture doit freiner
    routes = [[Espace(0, 0, 0, "non", "non"), Espace(1, 0, 0, "non", "non"), Espace(2, 0, 0, "non", "non")],[Espace(0, 1, 1, "droite", "non",10), Espace(1, 1, 1, "droite", "non", 10),Espace(2, 1, 0, "droite", "non", 10)],[Espace(0, 2, 0, "non", "non"), Espace(1, 2, 0, "non", "non"), Espace(2, 2, 0, "non", "non")]]
    v = rd.randint(2, 6)
    voitures = [[0 , 0, 0],[voiture((0, 1), v, routes, 0),voiture((1, 1), 1, routes, 0), 0],[0, 0, 0]]
    depart_route = [(0, 1)]
    fin_route = [(1, 2)]
    ajustement(voitures[1][0], voitures, routes)
    assert voitures[1][0].v == 1
    
def test_v_voiture_devant_pas_chgt():
    #teste la fonction ajustement si il y a une voiture devant et que la voiture doit garder la meme allure
    routes = [[Espace(0, 0, 0, "non", "non"), Espace(1, 0, 0, "non", "non"), Espace(2, 0, 0, "non", "non")],[Espace(0, 1, 1, "droite", "non",10), Espace(1, 1, 1, "droite", "non", 10),Espace(2, 1, 0, "droite", "non", 10)],[Espace(0, 2, 0, "non", "non"), Espace(1, 2, 0, "non", "non"), Espace(2, 2, 0, "non", "non")]]
    v = rd.randint(2, 6)
    voitures = [[0, 0, 0],[voiture((0, 1), v, routes ,0),voiture((1, 1), 8, routes, 0), 0],[0, 0, 0]]
    depart_route = [(0, 1)]
    fin_route = [(1, 2)]
    ajustement(voitures[1][0], voitures, routes)
    assert voitures[1][0].v == v

def test_avance_voiture_ligne_droite():
    #teste la fonction avancer_voitures en ligne droite
    routes = [[Espace(0,0, 0, "non", "non"), Espace(1,0, 0, "non", "non"), Espace(2, 0, 0, "non", "non")],[Espace(0, 1, 1, "droite", "non"), Espace(1, 1, 1, "droite", "non"),Espace(2, 1, 1, "droite", "non")],[Espace(0,2, 0, "non", "non"), Espace(1,2, 0, "non", "non"), Espace(2,2, 0, "non", "non")]]
    voitures = [[ 0,0,0],[voiture((0,1),2,routes),0,0],[0,0,0]]
    depart_route = [(0,1)]
    fin_route = [(1,2)]
    voitures = avancer_voitures(voitures, routes)
    assert voitures[1][2].position == (2, 1)


def test_avance_voiture_virage():
    #teste la fonction avancer_voitures si il y a des virages
    routes = [[Espace(0,0, 0, "non", "non"), Espace(1,0, 0, "non", "non"), Espace(2, 0, 0, "non", "non")],[Espace(0, 1, 1, "droite", "non"), Espace(1, 1, 1, "bas", "non"),Espace(2, 1, 0, "non", "non")],[Espace(0,2, 0, "non", "non"), Espace(1,2, 1, "bas", "non"), Espace(2,2, 0, "non", "non")]]
    routes[1][1].virage = "droite"
    voitures = [[ 0,0,0],[voiture((0,1),2,routes),0,0],[0,0,0]]
    depart_route = [(0,1)]
    fin_route = [(1,2)]
    voitures = avancer_voitures(voitures, routes)
    assert voitures[2][1].position == (1, 2)



    

if __name__ == "__main__":
    test_freinage_dans_virage()
    test_freinage_ligne_droite()
    test_arret_avt_inter()
    test_acc()
    test_v_voiture_devant()
    test_v_voiture_devant_pas_chgt()
    test_avance_voiture_ligne_droite()
    test_avance_voiture_virage()
    
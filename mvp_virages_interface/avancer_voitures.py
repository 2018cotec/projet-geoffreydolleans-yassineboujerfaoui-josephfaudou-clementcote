#fonction qui prend en argument l'univers avec les voitures et renvoie un nouvel univers avec les voitures dans leur nouvelle position
from Classes import *
from main import *

def avancer_voitures(position_voitures, position_routes):
    """
    :param position_voitures: liste des positions des voitures
    :param position_routes: liste des positions des routes
    :return: liste des voitures avec leur nouvelle position
    """
    taille = len(position_voitures)
    new_position_voiture = [[0 for _ in range(taille)] for _ in range (taille)] #créer un univers vide
    for ligne in position_voitures:
        for voiture in ligne:
            i = 0
            if voiture != 0:
                while i < voiture.v:
                    x, y = voiture.position
                    direction = voiture.direction
                    if direction == 'haut': #modifie la position de la voiture en fonction de sa direction et de sa vitesse
                        y -= 1
                    elif direction == 'bas':
                        y += 1
                    elif direction =='droite':
                        x += 1
                    else:
                        x -= 1
                    voiture.position = (x, y)
                    #il faut trouver la direction de la voiture
                    if 0 <= x <= taille - 1 and 0 <= y <= taille - 1 and  position_routes[y][x].type != 2:
                        voiture.direction = position_routes[y][x].direction
                    i += 1


                if 0 <= voiture.position[0] <= taille-1 and 0 <= voiture.position[1] <= taille-1 :
                    new_position_voiture[voiture.position[1]][voiture.position[0]]=voiture #ajoute la voiture au nouvel univers si elle est dedans

    return new_position_voiture

if __name__ == "__main__":
    #teste la fonction avancer_voitures si il y a des virages
    routes = [[Espace(0,0, 0, "non", "non"), Espace(1,0, 0, "non", "non"), Espace(2, 0, 0, "non", "non")],[Espace(0, 1, 1, "droite", "non"), Espace(1, 1, 1, "bas", "non"),Espace(2, 1, 0, "non", "non")],[Espace(0,2, 0, "non", "non"), Espace(1,2, 1, "bas", "non"), Espace(2,2, 0, "non", "non")]]
    routes[1][1].virage = "droite"
    voitures = [[ 0,0,0],[voiture((0,1),2,routes),0,0],[0,0,0]]
    depart_route = [(0,1)]
    fin_route = [(1,2)]
    voitures = avancer_voitures(voitures, routes)
    print(voitures)

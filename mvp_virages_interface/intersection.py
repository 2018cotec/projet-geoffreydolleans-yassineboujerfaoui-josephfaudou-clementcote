

def intersection(x,y,position_routes,position_voitures,n):
    """
    :param x: abscisse de l'intersection
    :param y: ordonnée de l'intersection
    :param position_routes: liste des routes
    :param position_voitures: liste des voitures
    :param n: numéro d'itération
    """


    intersection=position_routes[y][x]
    route_verticale=position_routes[y-1][x]
    route_horizontale=position_routes[y][x-1]

    #gère la priorité à droite
    if intersection.type_inter=='priorité à droite':

        if route_verticale.direction=='bas' and route_horizontale.direction=='droite': #1er cas : intersecton d'une route qui va vers la bas avec une route qui va vers la droite
            if position_voitures[y][x-1]!=0:
                voiture_prio=position_voitures[y][x-1]   #on définit la voiture prioritaire
                if position_voitures[y][x]==0:
                    voiture_prio.v=1                      #elle passe si l'intersection est libre
                else:
                    voiture_prio.v=0                        #sinon elle ne passe pas
            if position_voitures[y-1][x]!=0:            #on définit la voiture non prioritaire
                voiture_non_prio=position_voitures[y-1][x]
                if position_voitures[y][x-1]!=0 or position_voitures[y][x]!=0:  #elle ne passe pas si l'intersection est occupée ou s'il y a une voiture prioritaire
                    voiture_non_prio.v=0
                else :
                    voiture_non_prio.v=1                #sinon elle passe

        elif route_verticale.direction=='haut' and route_horizontale.direction=='droite':  #2ième cas
            if position_voitures[y+1][x]!=0:
                voiture_prio=position_voitures[y+1][x]
                if position_voitures[y][x]==0:
                    voiture_prio.v=1
                else:
                    voiture_prio.v=0
            if position_voitures[y][x-1]!=0:
                voiture_non_prio=position_voitures[y][x-1]
                if position_voitures[y+1][x]!=0 or position_voitures[y][x]!=0:
                    voiture_non_prio.v=0
                else :
                    voiture_non_prio.v=1

        elif route_verticale.direction=='bas' and route_horizontale.direction=='gauche':  #3ième cas
            if position_voitures[y-1][x]!=0:
                voiture_prio=position_voitures[y-1][x]
                if position_voitures[y][x]==0:
                    voiture_prio.v=1
                else:
                    voiture_prio.v=0
            if position_voitures[y][x+1]!=0:
                voiture_non_prio=position_voitures[y][x+1]
                if position_voitures[y-1][x]!=0 or position_voitures[y][x]!=0:
                    voiture_non_prio.v=0
                else :
                    voiture_non_prio.v=1

        elif route_verticale.direction=='haut' and route_horizontale.direction=='gauche':  #4ième et dernier cas
            if position_voitures[y][x+1]!=0:
                voiture_prio=position_voitures[y][x+1]
                if position_voitures[y][x]==0:
                    voiture_prio.v=1
                else:
                    voiture_prio.v=0
            if position_voitures[y+1][x]!=0:
                voiture_non_prio=position_voitures[y+1][x]
                if position_voitures[y][x+1]!=0 or position_voitures[y][x]!=0:
                    voiture_non_prio.v=0
                else :
                    voiture_non_prio.v=1

    #gère le feu rouge
    elif intersection.type_inter=='feu rouge':
        etape=n%20                      #période de 20 itérations

        if 0<=etape<=8:        #9 itérations feu vert pour les routes verticales

            if route_verticale.direction=='bas':
                if position_voitures[y-1][x] != 0 :
                    voiture=position_voitures[y-1][x]
                    if position_voitures[y][x]==0:              #une voiture sur une route verticale n'avance que si la place est libre
                        voiture.v=1
                    else:
                        voiture.v=0
            if route_verticale.direction=='haut':
                if position_voitures[y+1][x] != 0 :
                    voiture=position_voitures[y+1][x]
                    if position_voitures[y][x]==0:
                        voiture.v=1
                    else:
                        voiture.v=0

            if route_horizontale.direction=='gauche':              #une voiture sur une route horizontale s'arrête
                if position_voitures[y][x+1] != 0 :
                    voiture=position_voitures[y][x+1]
                    voiture.v=0
            if route_horizontale.direction=='droite':
                if position_voitures[y][x-1] != 0 :
                    voiture=position_voitures[y][x-1]
                    voiture.v=0

        elif 10<=etape<=18 :         #9 itérations feu vert pour les routes horizontales

            if route_verticale.direction=='bas':
                if position_voitures[y-1][x] != 0 :
                    voiture=position_voitures[y-1][x]
                    voiture.v=0
            if route_verticale.direction=='haut':
                if position_voitures[y+1][x] != 0 :
                    voiture=position_voitures[y+1][x]
                    voiture.v=0

            if route_horizontale.direction=='gauche':
                if position_voitures[y][x+1] != 0 :
                    voiture=position_voitures[y][x+1]
                    if position_voitures[y][x]==0:
                        voiture.v=1
                    else:
                        voiture.v=0
            if route_horizontale.direction=='droite':
                if position_voitures[y][x-1] != 0 :
                    voiture=position_voitures[y][x-1]
                    if position_voitures[y][x]==0:
                        voiture.v=1
                    else:
                        voiture.v=0

        elif etape==19 or etape==9 :                       #transition feu rouge/feu vert : les voitures des deux routes sont arrêtés
            if route_verticale.direction=='bas':
                if position_voitures[y-1][x] != 0 :
                    voiture=position_voitures[y-1][x]
                    voiture.v=0
            if route_verticale.direction=='haut':
                if position_voitures[y+1][x] != 0 :
                    voiture=position_voitures[y+1][x]
                    voiture.v=0
            if route_horizontale.direction=='gauche':
                if position_voitures[y][x+1] != 0 :
                    voiture=position_voitures[y][x+1]
                    voiture.v=0
            if route_horizontale.direction=='droite':
                if position_voitures[y][x-1] != 0 :
                    voiture=position_voitures[y][x-1]
                    voiture.v=0




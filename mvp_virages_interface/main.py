import tkinter as tk
import tkinter.ttk as ttk
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import generer_voiture
import Remonte_avec_virage
import avancer_voitures
import random as rd
import pickle
from matplotlib.colors import LinearSegmentedColormap
import time

#choix qu'on aura dans l'interface
densite_circulation={'Faible':0.02, 'Moyenne':0.1, 'Forte':0.3, 'Très forte':0.5, 'Bouchée':0.9}
vitesse_limite={'1':1, '2':2, '3':3}
type_intersection={'Priorité à droite':1,'Feu rouge':0,'Aléatoire':0.5,'Majoritairement feu rouge':0.25,'Majoritairement priorité à droite':0.75}

class Espace:
    """Classe qui définit une case d'espace caractérisée par :
    - sa position dans l'espace
    - son type (vide (0), route (1) ou intersection (2))
    - son type d'intersection ("non", "feu_rouge", "priorité_à_droite")
    - sa limitation de vitesse
    - si c'est un virage ou non (si c'est un virage : si c'est à droite ou à gauche)
    """

    def __init__(self, x, y, type, direction, type_inter, v_lim = 1, virage = None):
        self.position = (x, y)
        self.type = type
        self.type_inter = type_inter
        self.direction = direction
        self.v_lim = v_lim
        self.virage = virage


def generer_espace(taille, nombre_route,p,vlim):
    """
    :param taille: taille de l'espace que l'on va générer
    :param nombre_route: nombre de routes qui sera généré
    :param p: probabilité qu'une intersection soit une priorité à droite
    :param vlim: vitesse limite
    :return: routes : tableau de tableau contenant des objets espaces, depart_route : liste de tuples contenant les positions de départ des routes, fin_route : liste de tuples contenant les positions de fin des routes
    """

    # Génération du tableau
    routes = [[0 for i in range(taille)] for j in range(taille)]

    # Tableaux contenants respectivement les valeurs de départ sur x et sur y des routes
    x_start = []
    y_start = []

    # Tableaux contenants respectivement les coordonnées de départ et d'arrivée des routes
    depart_route = []
    fin_route = []
    # Tableaux contenant l'ordonné ou l'abscisse des virages pour éviter que deux routes se mélangent
    liste_x_virage, liste_y_virage = [], []

    for i in range(nombre_route):
        # Choix de la direction de la route
        depart = rd.choice(["x", "y"])

        if depart == 'x':
            # Choix du sens de la route
            x = rd.choice([0, taille - 1])

            # Choix de l'ordonnée de départ, et vérification qu'elle n'est pas adjacente à une route
            y = rd.randint(1, taille - 2)
            while y + 1 in y_start or y - 1 in y_start or y in y_start or y in liste_y_virage:
                y = rd.randint(1, taille - 2)

            # Ajout de la position de départ dans y_start
            y_start.append(y)

            # Fixe la valeur de la direction
            if x == 0:
                direction = "droite"
            else:
                direction = "gauche"

        # Equivalent au if précédent
        else:
            y = rd.choice([0, taille - 1])
            x = rd.randint(1, taille - 2)
            while x + 1 in x_start or x - 1 in x_start or x in x_start or x in liste_x_virage:
                x = rd.randint(1, taille - 2)
            x_start.append(x)
            if y == 0:
                direction = "bas"
            else:
                direction = "haut"

        # Ajout du point de départ de la route
        depart_route.append((x, y))

        # Ajout d'une distance maximale pour ne pas avoir de boucle infinie
        distance = 100 * taille
        j = 0
        k = 0  #compteur de virage
        # Boucle jusqu'à ce que la route touche le bout de l'espace
        while x < taille and y < taille and x >= 0 and y >= 0 and j < distance:

            # S'il n'y a pas déjà de route : crée une route
            if routes[y][x] == 0:
                routes[y][x] = Espace(x, y, 1, direction, "non",vlim)

            # S'il y a déjà une route, transforme la route existante en intersection
            else:
                routes[y][x].type = 2
                routes[y][x].direction = "non"
                if rd.random() < p:
                    routes[y][x].type_inter = "priorité à droite"
                else:
                    routes[y][x].type_inter = "feu rouge"

            def virage_gauche(direction) :
                 if direction == "haut" :
                      return "gauche"
                 elif direction == "bas" :
                      return "droite"
                 elif direction == "gauche" :
                      return "bas"
                 elif direction == "droite" :
                      return "haut"

            def virage_droit(direction) :
                 if direction == "haut" :
                      return "droite"
                 elif direction == "bas" :
                      return "gauche"
                 elif direction == "gauche" :
                      return "haut"
                 elif direction == "droite" :
                      return "bas"

            def direction_apres(direction_avant, virage):
                 if virage == "droite":
                      return virage_droit(direction_avant)
                 elif virage == "gauche":
                      return virage_gauche(direction_avant)

            def direction_deplacement(direction):
                if direction == "haut" :
                    return (0,-1)
                elif direction == "gauche" :
                    return (-1,0)
                elif direction == "droite" :
                    return (1,0)
                elif direction == "bas" :
                    return (0,1)

            direc_dep = direction_deplacement(direction)
            if routes[y][x].type != 2:
                #pas de virage a une intersection
                a, b = x + direc_dep[0], y + direc_dep[1]
                c, d = x - direc_dep[0], y - direc_dep[1]
                if 0<= a<= taille - 1 and 0 <= b <= taille - 1 and 0<= c<= taille - 1 and 0 <= d <= taille - 1 and routes[b][a] == 0 and routes[d][c] !=2 :
                    #Si la route pourrait se mélanger ou se coller a une autre route on ne tourne pas
                    if x not in liste_x_virage and y not in liste_y_virage:
                        #Traite le cas ou la route est initiallement horizontale
                        if direction in ["droite", "gauche"]:
                            if x not in x_start and x + 1 not in x_start and x - 1 not in x_start:
                                if k<1 and x != 0 and x != taille - 1 and y!=0 and y!= taille -1 :
                                    changement_direction = rd.random()
                                    if changement_direction < .05 :
                                        virage = rd.choice(["gauche","droite"])
                                        direction = direction_apres(direction, virage)
                                        #on doit retenir l'ordonné du virage et ses voisins pour eviter aussi deux routes collées
                                        liste_y_virage.append(y-1)
                                        liste_y_virage.append(y)
                                        liste_y_virage.append(y+1)
                                        liste_x_virage.append(x-1)
                                        liste_x_virage.append(x)
                                        liste_x_virage.append(x+1)


                                        routes[y][x].direction = direction
                                        routes[y][x].virage = virage
                                        k+=1
                        #Traite le cas ou la route est verticale
                        else    :
                            if y not in y_start and y + 1 not in y_start and y - 1 not in y_start:
                                if k<1 and x != 0 and x != taille - 1 and y!=0 and y!= taille -1 :
                                    changement_direction = rd.random()
                                    if changement_direction < .05 :
                                        virage = rd.choice(["gauche","droite"])
                                        direction = direction_apres(direction, virage)
                                        #on doit retenir l'ordonné du virage et ses voisins pour eviter aussi deux routes collées
                                        liste_y_virage.append(y-1)
                                        liste_y_virage.append(y)
                                        liste_y_virage.append(y+1)
                                        liste_x_virage.append(x-1)
                                        liste_x_virage.append(x)
                                        liste_x_virage.append(x+1)


                                        routes[y][x].direction = direction
                                        routes[y][x].virage = virage
                                        k+=1

            if direction == "haut":
                    y -= 1
            elif direction == "bas":
                    y += 1
            elif direction == "gauche":
                    x -= 1
            elif direction == "droite":
                    x += 1

            j += 1

        # Ajout de la fin de la route : dépend de la direction
        if direction == "haut":
            fin_route.append((x, y + 1))
        elif direction == "bas":
            fin_route.append((x, y - 1))
        elif direction == "gauche":
            fin_route.append((x + 1, y))
        elif direction == "droite":
            fin_route.append((x - 1, y))

        # On remplit les cases qui ne sont pas des routes avec des espaces de type 0
    for x in range(taille):
        for y in range(taille):
            if routes[y][x] == 0:
                routes[y][x] = Espace(x, y, 0, "non", "non")

    return routes, depart_route, fin_route


def generation_image(position_routes, position_voitures):
    """
    :param position_routes: liste de liste contenant des objets de type espace, donnant la position des routes et des intersections
    :param position_voitures: liste de liste contenant des objets de type voiture ou des espaces
    :return: un tableau "image" des routes et des voitures, interprétable par plt.imshow
    """

    # Récupération de la taille de l'univers
    taille = len(position_routes)

    # Création du tableau image
    image = [[0 for i in range(10 * taille)] for j in range(10 * taille)]

    # Parcours des tableaux position_routes et position_images
    for i in range(taille):
        for j in range(taille):

            # S'il y a une route, on dessine un carré
            if position_routes[j][i].type == 1:
                for k in range(10):
                    image[10 * j][10 * i + k] = 3
                    image[10 * j + k][10 * i] = 3
                    image[10 * j + 9][10 * i + k] = 3
                    image[10 * j + k][10 * i + 9] = 3

            # S'il y a une intersection
            elif position_routes[j][i].type == 2:
                # Si c'est une priorité à droite on affiche en vert
                if position_routes[j][i].type_inter=="priorité à droite":
                    for k in range(10):
                        image[10 * j][10 * i + k] = 1
                        image[10 * j + k][10 * i] = 1
                        image[10 * j + 9][10 * i + k] = 1
                        image[10 * j + k][10 * i + 9] = 1
                # Sinon (un feu rouge) on affiche en rouge
                else:
                    for k in range(10):
                        image[10 * j][10 * i + k] = 2
                        image[10 * j + k][10 * i] = 2
                        image[10 * j + 9][10 * i + k] = 2
                        image[10 * j + k][10 * i + 9] = 2

            # S'il y a une voiture, on remplit le carré
            if position_voitures[j][i] != 0:
                for k in range(1,9):
                    for p in range(1,9):
                        image[10 * j + p][10 * i + k] = 3
    return image


def next_step(position_routes, position_voitures, depart_routes, fin_routes, n,proba_apparition):
    """
    :param position_routes: liste de liste contenant des objets de type espace, donnant la position des routes et des intersections
    :param position_voitures: liste de liste contenant des objets de type voiture ou des espaces
    :param depart_routes: liste de tuples contenant les positions de départ des routes
    :param fin_routes: liste de tuples contenant les positions de fin des routes
    :param proba_apparition: probabilité qu'une voiture arrive au début d'une route
    :param n: numéro de l'itération
    :return: le tableau position_voitures à l'itération suivante
    """

    # On génère les voitures qui apparaissent au début des routes
    generer_voiture.generer_voiture(position_voitures, depart_routes, position_routes, proba_apparition)

    # On régle la vitesse de toutes les voitures
    Remonte_avec_virage.remonte_route(fin_routes, depart_routes, position_routes, position_voitures, n)

    # On fait avancer toutes les voitures selon leur vitesses
    position_voitures = avancer_voitures.avancer_voitures(position_voitures,position_routes)

    return position_voitures














if __name__=="__main__":

    #fonction pour lancer la simulation
    def lancer_simulation():
        #on récupère les données entrées dans l'interface
        global position_voitures, position_routes, depart_routes, fin_routes, n, proba_apparition
        taille = int(taille_text.get())
        v_lim = int(vitesse_limite[vlim_list.get()])
        proba_apparition = float(densite_circulation[densite_list.get()])
        proba_inter = float(type_intersection[intersection_list.get()])
        nb_routes= int(route_text.get())
        n = 0
        #on génère l'espace de départ
        position_routes, depart_routes, fin_routes = generer_espace(taille, nb_routes,proba_inter,v_lim)
        position_voitures = [[0 for _ in range(taille)] for _ in range(taille)]
        generer_voiture.generer_voiture(position_voitures, depart_routes, position_routes, 0.3) #dans l'univers de départ: probabilité de 0.3 d'y avoir une voiture au déoart d'une route

        # Création de la colormap personnalisée
        colormap=LinearSegmentedColormap.from_list('CustomColormap',[(1,1,1),(0,1,0),(1,0,0),(0,0,0)])

        # Création de la figure et de la première image de l'animation
        fig = plt.figure()
        im = plt.imshow(generation_image(position_routes, position_voitures), cmap=colormap, animated=True)


        # Définition de la fonction d'animation
        def updatefig(*args):
            global position_voitures, position_routes, depart_routes, fin_routes, n, proba_apparition

            position_voitures = next_step(position_routes, position_voitures, depart_routes, fin_routes, n,proba_apparition)
            im.set_array(generation_image(position_routes, position_voitures))
            time.sleep(0.5)
            # Itération suivante
            n += 1
            return im,


        ani = animation.FuncAnimation(fig, updatefig, interval=300, blit=True,repeat=False)
        plt.axis('off') #on enlève les axes
        plt.show()



    window = tk.Tk()  # créer une fenêtre tkinter
    window.title("Simulation du trafic routier")
    window.resizable(False, False)  # empêche de redimensionner la fenêtre


    frame_global = tk.Frame(window, padx=10, pady=10, bg='white') #fenêtre principale

    instruction = tk.Label(frame_global, text="Entrer les paramètres", font=("Calibri", 25, 'bold'), pady=5,
                         bg='white')  # font modifie la police et la taille d'écriture
    instruction.grid(row=0, column=0)

    frame1 = tk.Frame(frame_global, pady=5, bg='white') #sous fenêtre dans la fenêtre principale

    taille_text = tk.StringVar(frame1)  # permet d'entrer la taille de l'univers
    taille_text.set("50")  # valeur par défaut
    taille_label = tk.Label(frame1, text="Taille de l'univers", font=("Calibri", 14), bg='white')
    taille_label.grid(row=0, column=0)
    taille_entry = tk.Entry(frame1, textvariable=taille_text, width=30, font=("Calibri", 14), bg='white')
    taille_entry.grid(row=0, column=1)


    # ajoute menu déroulant pour choisir la densité de circulation
    densite_label = tk.Label(frame1, text="Densité de circulation", font=("Calibri", 14), bg='white')
    densite_label.grid(row=2, column=0)
    densite_list = ttk.Combobox(frame1, values=tuple(densite_circulation.keys()), width=28, font=("Calibri", 14), background='white')
    densite_list.current(2)
    densite_list.grid(row=2, column=1)

    #ajoute menu déroulant pour sélectionner la vitesse limite
    vlim_label = tk.Label(frame1, text="Vitesse limite", font=("Calibri", 14), bg='white')
    vlim_label.grid(row=3, column=0)
    vlim_list = ttk.Combobox(frame1, values=tuple(vitesse_limite.keys()), width=28, font=("Calibri", 14), background='white')
    vlim_list.current(2)
    vlim_list.grid(row=3, column=1)

    #ajoute menu déroulant pour sélectionner le type d'intersection
    intersection_label = tk.Label(frame1, text="Type d'intersection", font=("Calibri", 14), bg='white')
    intersection_label.grid(row=4, column=0)
    intersection_list = ttk.Combobox(frame1, values=tuple(type_intersection.keys()), width=28, font=("Calibri", 14), background='white')
    intersection_list.current(2)
    intersection_list.grid(row=4, column=1)

    route_text = tk.StringVar(frame1)  # permet d'entrer le nombre de route
    route_text.set("6")  # valeur par défaut
    route_label = tk.Label(frame1, text="Nombre de routes", font=("Calibri", 14), bg='white')
    route_label.grid(row=1, column=0)
    route_entry = tk.Entry(frame1, textvariable=route_text, width=30, font=("Calibri", 14), bg='white')
    route_entry.grid(row=1, column=1)



    button = tk.Button(frame_global, text="Lancer la simulation", activebackground="white", fg="black",
                       command=lancer_simulation, font=("Calibri", 14), bg='white',
                       relief='groove')  # bouton pour activer la fonction qui active la simulation

    frame1.grid(row=1,column=0)
    button.grid(row=2, column=0)

    frame_global.grid()

    window.mainloop()  # ouvre la fenêtre

import matplotlib.pyplot as plt
import Espace
import generer_voiture

def affichage(position_routes, position_voitures):
    taille=len(position_routes)
    image=[[0 for i in range(10*taille)] for j in range(10*taille)]
    for i in range(taille):
        for j in range(taille):
            if position_routes[j][i].type!=0:
                for k in range(10):
                    image[10*j][10*i+k]=1
                    image[10*j+k][10*i]=1
                    image[10*j+9][10*i+k]=1
                    image[10*j+k][10*i+9]=1
            if position_voitures[j][i] != 0:
                for k in range (10):
                    for p in range (10):
                        image[10*j+p][10*i+k] = 1
                        
    plt.imshow(image,cmap="Greys")
    plt.show()

if __name__=='__main__':
    position_voitures = [[0 for _ in range(50)] for _ in range (50)]
    position_routes, depart_route, _=Espace.generer_espace(50,8)
    generer_voiture.generer_voiture(position_voitures,depart_route,position_routes,1/2)
    affichage(position_routes,position_voitures)

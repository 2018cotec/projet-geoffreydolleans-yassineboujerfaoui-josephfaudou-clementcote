#fonction qui prend en argument l'univers avec les voitures et renvoie un nouvel univers avec les voitures dans leur nouvelle position

def avancer_voitures(position_voitures, position_routes):
    taille = len(position_voitures)
    new_position_voiture = [[0 for _ in range(taille)] for _ in range (taille)] #créer un univers vide
    for ligne in position_voitures:
        for voiture in ligne:
            i = 0
            if voiture != 0:
                while i < voiture.v:
                    x, y = voiture.position
                    direction = voiture.direction
                    if direction == 'haut': #modifie la position de la voiture en fonction de sa direction et de sa vitesse
                        y -= 1
                    elif direction == 'bas':
                        y += 1
                    elif direction =='droite':
                        x += 1
                    else:
                        x -= 1
                    voiture.position = (x, y)
                    #il faut trouver la direction de la voiture
                    if position_routes[y][x].etat == 2:

                    else:
                        dir_route = position_routes[y][x].direction
                        if dir_route in ["haut", "bas", "droite", "gauche"]:
                            voiture.direction = dir_route
                        else:
                            #ajuste la direction de la voiture en cas de virage
                            if dir_route == "gauche":
                                if direction == "haut":
                                    voiture.direction = "gauche"
                                elif direction == "bas":
                                    voiture.direction = "droite"
                                elif direction == "gauche":
                                    voiture.direction = "bas"
                                else:
                                    voiture.direction = "haut"
                            else:
                                if direction == "haut":
                                    voiture.direction = "droite"
                                elif direction == "bas":
                                    voiture.direction = "gauche"
                                elif direction == "gauche":
                                    voiture.direction = "haut"
                                else:
                                    voiture.direction = "bas"



                if 0<=voiture.x<=taille-1 and 0<=voiture.y<=taille-1 :
                    new_position_voiture[voiture.y][voiture.x]=voiture #ajoute la voiture au nouvel univers si elle est dedans

    return new_position_voiture

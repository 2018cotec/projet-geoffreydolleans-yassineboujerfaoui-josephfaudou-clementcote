import random as rd

def direction_deplacement(espace) :
    #Fonction qui retourne le couple de déplacement en fonction de la direction de la route
    if espace.direction == "haut" :
        return (0,-1)
    if espace.direction == "gauche" :
        return (-1,0)
    if espace.direction == "droite" :
        return (1,0)
    if espace.direction == "bas" :
        return (0,1)

def voiture_suivante(voiture,position_voitures,position_routes) :
    """:param voiture : Classe voiture pour laquelle on va retrouver la voiture qui la suit
    :param position_voitures : Liste de listes ayant comme éléments les objets voitures
    :param position_routes : Liste de listes ayant comme éléments les objets routes
    :return Soit None , soit une chaine de caractère qui signale la présence d'une intersection devant la voiture avec le nombre de cases entre la voiture
    et l'intersection et l'objet route qui se trouve à l'intersection"""
    x,y = voiture.position
    i = 0 #Initialisation du compteur de nombre de cases séparant la voiture et la voiture qui la suit
    while i < voiture.v :
        try:
            #On se déplace selon la direction de la case de coordonnées (x,y)
            x, y = x + direction_deplacement(position_routes[y][x])[0], y + direction_deplacement(position_routes[y][x])[1]
            #Si la route est de type intersection , on arrête la voiture juste avant l'intersection
            if position_routes[y][x].type == 2 :
                return "intersection",i
            #Si la route contient une voiture suivante , on retourne la voiture qui la suit avec le nombre de cases les séparants
            if position_voitures[y][x] != 0 :
                return "voiture_suiv",i ,position_voitures[y][x]
            i += 1
        except IndexError:
            #S'il n'y a ni voiture ni intersection , on ne retourne rien
            return None
        except:
            return None
            
    return None

def ajustement(voiture,position_voitures,position_routes) :
    #Fonction qui ajuste la vitesse de la voiture
    voit_suiv = voiture_suivante(voiture,position_voitures,position_routes) #On récupère la voiture ou l'intersection qui suit notre voiture
    if voit_suiv != None :
        #On distingue les deux cas où on a une voiture ou une intersection
        if voit_suiv[0] == "intersection" :
            voiture.v = voit_suiv[1]
        elif  voiture.v - voit_suiv[-1].v > voit_suiv[1] :
            voiture.v = voit_suiv[-1].v
    else:
        if voiture.v !=0:
            if rd.random() < voiture.proba_freinage:
                voiture.v -= 1
            elif voiture.v < position_routes[voiture.position[1]][voiture.position[0]].v_lim :
                #La voiture accelère si elle est inférieur a la vitesse limite a moins qu'elle rencontre un problème si elle accélère
                voiture.v += 1
                if voiture_suivante(voiture, position_voitures, position_routes) != None:
                    voiture.v -= 1
        else:
            if voiture.v < position_routes[voiture.position[1]][voiture.position[0]].v_lim :
                #La voiture accelère a moins qu'elle rencontre un problème si elle accélère
                voiture.v += 1
                if voiture_suivante(voiture, position_voitures, position_routes) != None:
                    voiture.v -= 1





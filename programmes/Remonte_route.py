from intersection import *
from Fonctions import *


def direction_remonter(espace):
    if espace.direction == "haut":
        return 0, +1
    elif espace.direction == "bas":
        return 0, -1
    elif espace.direction == "droite":
        return -1, 0
    else:
        return +1, 0
        

def Changement_vitesse(voiture, position_voitures, position_routes,n):
    x,y = voiture.position
    x, y = x - direction_remonter(position_routes[y][x])[0], y - direction_remonter(position_routes[y][x])[1]
    if position_routes[x][y] == 2:
        intersection(x, y,position_routes,position_voitures,n)
    else:
        ajustement(voiture, position_voitures, position_routes)


def remonte_route(fin_route, depart_route, position_routes, position_voitures,n):
    for pos in fin_route:
        x,y=pos[0],pos[1]
        direction = direction_remonter(position_routes[y][x])
        x, y = x + direction[0], y + direction[1]
        while (x,y) not in depart_route:
            if position_routes[y][x].type !=2:
                direction = direction_remonter(position_routes[y][x])
            x, y = x + direction[0], y + direction[1]
            if position_voitures[y][x] != 0:
                Changement_vitesse(position_voitures[y][x],position_voitures,position_routes,n)

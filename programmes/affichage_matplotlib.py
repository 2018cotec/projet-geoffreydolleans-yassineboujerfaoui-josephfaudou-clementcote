import matplotlib.pyplot as plt
import Espace

def affichage(position_routes):
    taille=len(position_routes)
    image=[[0 for i in range(10*taille)] for j in range(10*taille)]
    for i in range(taille):
        for j in range(taille):
            if position_routes[j][i].type!=0:
                for k in range(10):
                    image[10*j][10*i+k]=1
                    image[10*j+k][10*i]=1
                    image[10*j+9][10*i+k]=1
                    image[10*j+k][10*i+9]=1
    plt.imshow(image,cmap="Greys")
    plt.show()

if __name__=='__main__':
    positions_routes=Espace.generer_espace(50,8)[0]
    affichage(positions_routes)

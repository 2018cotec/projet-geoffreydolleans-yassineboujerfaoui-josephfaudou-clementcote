from Espace import *
from generer_voiture import *
from Remonte_route import *
from avancer_voitures import *
from Classes import *
from Aff_route_voiture import *

def main():
    #création de l'univers et des différente listes
    taille = input("Entrer la taille de la grille ")
    nb_routes = input("Entrer le nombre de routes")
    position_routes, depart_route, fin_route = generer_espace(taille, nb_routes)
    voitures = []
    position_voitures = [[0 for i in range (taille)] for j in range (taille)]
    generer_voiture(.5)
    n=0
    while True:
        #Animation a ajouter
        
        generer_voiture(position_voitures,depart_route,position_routes,0.3)
        remonte_route(fin_route, depart_route, position_routes, position_voitures,n)
        position_voitures = avancer_voitures(position_voitures)
        n+=1

import Classes
import avancer_voitures
import Espace
import pytest

position_routes=[[0 for _ in range(3)] for _ in range(3)]
position_voitures=[[0 for _ in range(3)] for _ in range(3)]

#on crée le tableau position_routes
for i in range(3):
    route_horizontale=Espace.Espace(i,1,1,"droite","non",v_lim=3, p_arrivee=0)
    route_verticale=Espace.Espace(1,i,1,"bas","non",v_lim=3, p_arrivee=0)
    position_routes[i][1]=route_verticale
    position_routes[1][i]=route_horizontale

intersection=Espace.Espace(1,1,2,"bas","priorité à droite",v_lim=3, p_arrivee=0)

#On crée le tableau prosition_voitures
voiture1=Classes.voiture((1,0),0,position_routes,proba_freinage=0)
position_voitures[0][1]=voiture1
voiture2=Classes.voiture((0,1),1,position_routes,proba_freinage=0)
position_voitures[1][0]=voiture2



#ce que doit retourner avancer_voitures
new_position_voitures=[[0 for _ in range(3)] for _ in range(3)]
new_position_voitures[0][1]=voiture1
voiture2_2=Classes.voiture((1,1),1,position_routes,proba_freinage=0)
new_position_voitures[1][1]=voiture2

def test_avancer_voitures():
    assert avancer_voitures.avancer_voitures(position_voitures)==new_position_voitures

if __name__=='__main__':
    test_avancer_voitures()

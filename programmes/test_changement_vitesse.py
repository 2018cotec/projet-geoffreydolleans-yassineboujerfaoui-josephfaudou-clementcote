
import avancer_voitures
import Espace
import Remonte_avec_virage
import pytest
import copy

from Fonctions import *

class voiture :
    def __init__(self,pos,v,position_routes,proba_freinage=0):
        self.position=pos
        self.v=v
        self.proba_freinage=proba_freinage
        self.direction=position_routes[pos[1]][pos[0]].direction

position_routes=[[0 for _ in range(3)] for _ in range(3)]
position_voitures=[[0 for _ in range(3)] for _ in range(3)]


#on crée la liste position routes
for i in range(3):
    route_horizontale=Espace.Espace(i,1,1,"droite","non",v_lim=3, p_arrivee=0)
    position_routes[1][i]=route_horizontale

intersection=Espace.Espace(1,1,2,"bas","priorité à droite",v_lim=3, p_arrivee=0)

#on crée la liste position_voitures
voiture1=voiture((0,1),2,position_routes,proba_freinage=0)
position_voitures[1][0]=voiture1

def intersection(x,y,position_routes,position_voitures,n):
    """
    :param x: abscisse de l'intersection
    :param y: ordonnée de l'intersection
    :param position_routes: liste des routes
    :param position_voitures: liste des voitures
    :param n: numéro d'itération
    """

    intersection=position_routes[y][x]
    route_verticale=position_routes[y-1][x]
    route_horizontale=position_routes[y][x-1]

    #gère la priorité à droite
    if intersection.type_inter=='priorité à droite':

        if route_verticale.direction=='bas' and route_horizontale.direction=='droite': #1er cas : intersecton d'une route qui va vers la bas avec une route qui va vers la droite
            if position_voitures[y][x-1]!=0:
                voiture_prio=position_voitures[y][x-1]   #on définit la voiture prioritaire
                if position_voitures[y][x]==0:
                    voiture_prio.v=1                      #elle passe si l'intersection est libre
                else:
                    voiture_prio.v=0                        #sinon elle ne passe pas
            if position_voitures[y-1][x]!=0:            #on définit la voiture non prioritaire
                voiture_non_prio=position_voitures[y-1][x]
                if position_voitures[y][x-1]!=0 or position_voitures[y][x]!=0:  #elle ne passe pas si l'intersection est occupée ou s'il y a une voiture prioritaire
                    voiture_non_prio.v=0
                else :
                    voiture_non_prio.v=1                #sinon elle passe

        if route_verticale.direction=='haut' and route_horizontale.direction=='droite':  #2ième cas
            if position_voitures[y+1][x]!=0:
                voiture_prio=position_voitures[y+1][x]
                if position_voitures[y][x]==0:
                    voiture_prio.v=1
                else:
                    voiture_prio.v=0
            if position_voitures[y][x-1]!=0:
                voiture_non_prio=position_voitures[y][x-1]
                if position_voitures[y+1][x]!=0 or position_voitures[y][x]!=0:
                    voiture_non_prio.v=0
                else :
                    voiture_non_prio.v=1

        if route_verticale.direction=='bas' and route_horizontale.direction=='gauche':  #3ième cas
            if position_voitures[y-1][x]!=0:
                voiture_prio=position_voitures[y-1][x]
                if position_voitures[y][x]==0:
                    voiture_prio.v=1
                else:
                    voiture_prio.v=0
            if position_voitures[y][x+1]!=0:
                voiture_non_prio=position_voitures[y][x+1]
                if position_voitures[y-1][x]!=0 or position_voitures[y][x]!=0:
                    voiture_non_prio.v=0
                else :
                    voiture_non_prio.v=1

        if route_verticale.direction=='haut' and route_horizontale.direction=='gauche':  #4ième et dernier cas
            if position_voitures[y][x+1]!=0:
                voiture_prio=position_voitures[y][x+1]
                if position_voitures[y][x]==0:
                    voiture_prio.v=1
                else:
                    voiture_prio.v=0
            if position_voitures[y+1][x]!=0:
                voiture_non_prio=position_voitures[y+1][x]
                if position_voitures[y][x+1]!=0 or position_voitures[y][x]!=0:
                    voiture_non_prio.v=0
                else :
                    voiture_non_prio.v=1

    #gère le feu rouge
    if intersection.type_inter=='feu rouge':
        etape=n%20                      #période de 20 itérations

        if 0<=etape<=8:        #9 itérations feu vert pour les routes verticales

            if route_verticale.direction=='bas':
                if position_voitures[y-1][x] != 0 :
                    voiture=position_voitures[y-1][x]
                    if position_voitures[y][x]==0:              #une voiture sur une route verticale n'avance que si la place est libre
                        voiture.v=1
                    else:
                        voiture.v=0
            if route_verticale.direction=='haut':
                if position_voitures[y+1][x] != 0 :
                    voiture=position_voitures[y+1][x]
                    if position_voitures[y][x]==0:
                        voiture.v=1
                    else:
                        voiture.v=0

            if route_horizontale.direction=='gauche':              #une voiture sur une route horizontale s'arrête
                if position_voitures[y][x+1] != 0 :
                    voiture=position_voitures[y][x+1]
                    voiture.v=0
            if route_horizontale.direction=='droite':
                if position_voitures[y][x-1] != 0 :
                    voiture=position_voitures[y][x-1]
                    voiture.v=0

        if 10<=etape<=18 :         #9 itérations feu vert pour les routes horizontales

            if route_verticale.direction=='bas':
                if position_voitures[y-1][x] != 0 :
                    voiture=position_voitures[y-1][x]
                    voiture.v=0
            if route_verticale.direction=='haut':
                if position_voitures[y+1][x] != 0 :
                    voiture=position_voitures[y+1][x]
                    voiture.v=0

            if route_horizontale.direction=='gauche':
                if position_voitures[y][x+1] != 0 :
                    voiture=position_voitures[y][x+1]
                    if position_voitures[y][x]==0:
                        voiture.v=1
                    else:
                        voiture.v=0
            if route_horizontale.direction=='droite':
                if position_voitures[y][x-1] != 0 :
                    voiture=position_voitures[y][x-1]
                    if position_voitures[y][x]==0:
                        voiture.v=1
                    else:
                        voiture.v=0

        if etape==19 or etape==9 :                       #transition feu rouge/feu vert : les voitures des deux routes sont arrêtés
            if route_verticale.direction=='bas':
                if position_voitures[y-1][x] != 0 :
                    voiture=position_voitures[y-1][x]
                    voiture.v=0
            if route_verticale.direction=='haut':
                if position_voitures[y+1][x] != 0 :
                    voiture=position_voitures[y+1][x]
                    voiture.v=0
            if route_horizontale.direction=='gauche':
                if position_voitures[y][x+1] != 0 :
                    voiture=position_voitures[y][x+1]
                    voiture.v=0
            if route_horizontale.direction=='droite':
                if position_voitures[y][x-1] != 0 :
                    voiture=position_voitures[y][x-1]
                    voiture.v=0


def Changement_vitesse(voiture, position_voitures, position_routes,n):
    """ajuste la vitesse de la voiture pour eviter toute collision, s'arreter avant les intersections et gere tout type d'intersection"""
    x,y = voiture.position

    #si on est sur une intersection, on ajuste la vitesse de la voiture
    if position_routes[x][y] == 2:
        ajustement(voiture, position_voitures, position_routes)
    else:
        #on va voir la prochaine case
        direction = voiture.direction
        if direction == "haut":
            y -=1
        elif direction == "bas":
            y +=1
        elif direction == "droite":
            x += 1
        elif direction == "gauche":
            x -= 1
        try:
            #si la prochaine case est un intersection appelle la f° intersection
            if position_routes[y][x].type == 2:
                intersection(x, y,position_routes,position_voitures,n)
            #sinon verifie si la voiture doit accelerer ou freiner
            else:
                ajustement(voiture, position_voitures, position_routes)
        except IndexError:
            pass
    return (position_voitures) #obligation d'ajouter un return pour tester

#ce que doit retourner avancer_voitures
new_position_voitures=[[0 for _ in range(3)] for _ in range(3)]

voiture1_2=copy.deepcopy(voiture1)
voiture1_2.v=2

new_position_voitures[1][0]=voiture1

def test_changement_vitesse():
    position_voitures2=Changement_vitesse(voiture1,position_voitures,position_routes,n=0)
    assert new_position_voitures==position_voitures2

if __name__=='__main__':
    test_changement_vitesse()
